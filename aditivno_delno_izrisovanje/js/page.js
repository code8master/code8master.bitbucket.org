var opencvLoadedFlag = false;
var canvas = null;
var canvas_2d_context = null;
var original_image = null;
var img = null;
var src = null;
var size = null;
var i = 1;
var sleep = 1000;
var alpha_devison = 2;

//------------------------------------------------------------------------------------
function opencvLoaded() {
	// document.querySelector("#status").innerHTML = "OpenCV.js is ready.";
	opencvLoadedFlag = true;
}
//------------------------------------------------------------------------------------

function print(el) {
	console.log(el);
}
//------------------------------------------------------------------------------------
function error(el) {
	console.error(el);
}

window.onload = function () {
	// print(window.location.href)
    img = new Image();
	if (window.location.href.endsWith("/ivana/")){
		arr = ["./ivana_happy.jpg","./ivana_angry.jpg","./drawing.jpg"];
	}
	else {
		arr = ["./bird.jpg","./lena.png","./folder.jpg"]; 
	}
	indx = Math.floor(Math.random()*arr.length);
	img.src = arr[indx];
    canvas = document.getElementById("target_canvas")
    // canvas.width = document.documentElement.clientWidth;
    // canvas.height = document.documentElement.clientHeight;
	if (opencvLoadedFlag == false) {
		error("OpenCV hasn't been loadaed yet !!!");
	}
	print("Window Loaded");
	cv['onRuntimeInitialized'] = () => {
		// do all your work here
		setTimeout(mainF, 100);
	};
}


function mainF(){
    src = cv.imread(img);
    size = src.size()
    if (img.width>2000 && img.height>2000){
		canvas.width = Math.floor(img.width/2);
		canvas.height = Math.floor(img.height/2);
    }
    else {
		canvas.width = img.width;
		canvas.height = img.height;
    }
	canvas_2d_context = canvas.getContext("2d");
    canvas_2d_context.clearRect(0, 0, canvas.width, canvas.height);
    
	setInterval(call_iterations,sleep);
}

function call_iterations(){
	draw_iteration(i-1,i); // + row
	draw_iteration(i,i-1); // + column
	draw_iteration(i,i); // + row + column
	i++;
}
function draw_iteration(rows, columns){
    for (let j = 0; j<rows*columns; j++){
        column_indx = j%columns;
        row_indx = Math.floor(j/columns);
        
        up_left_x = size.width/columns*column_indx;
        up_left_y = size.height/rows*row_indx;
        down_right_x = size.width/columns*(column_indx+1);
        down_right_y = size.height/rows*(row_indx+1);
        
        temp = new cv.Rect(up_left_x,up_left_y, down_right_x-up_left_x,down_right_y-up_left_y);
        dst = src.roi(temp);
        mean = cv.mean(dst);
        dst.delete();
        color = "#"+Math.floor(mean[0]).toString(16)+Math.floor(mean[1]).toString(16)+Math.floor(mean[2]).toString(16)+Math.floor(mean[3]/alpha_devison).toString(16);
        
        start_perc_x = up_left_x/size.width;
        start_perc_y = up_left_y/size.height;
        end_perc_x = down_right_x/size.width;
        end_perc_y = down_right_y/size.height;

        c_start_x = canvas_2d_context.canvas.width*start_perc_x;
        c_start_y = canvas_2d_context.canvas.height*start_perc_y;
        c_end_x = canvas_2d_context.canvas.width*end_perc_x;
        c_end_y = canvas_2d_context.canvas.height*end_perc_y;
        
        centerX = (c_start_x+c_end_x)/2;
        centerY = (c_start_y+c_end_y)/2;
        radius = Math.min(c_end_x-c_start_x, c_end_y-c_start_y)/2;
        
		if (rows>columns){
			timeToSleep = Math.random()*sleep/3;
		}
		else if (rows<columns){
			timeToSleep = sleep/3+Math.random()*sleep/3;
		}
		else {
			timeToSleep = 2*sleep/3+Math.random()*sleep/3;
		}
        setTimeout(draw_circle,timeToSleep,canvas_2d_context, centerX, centerY, radius,color+"");
		// draw_circle(canvas_2d_context, centerX, centerY, radius,color+"");
    }
}

function draw_circle(ctx, x, y, radius, fill, stroke, strokeWidth) {
    ctx.beginPath()
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false)
    if (fill) {
      ctx.fillStyle = fill
      ctx.fill()
    }
    if (stroke) {
      ctx.lineWidth = strokeWidth
      ctx.strokeStyle = stroke
      ctx.stroke()
    }
  }