var editor;
var engine;
var container;
var components;

var blackChannel;
var whiteChannel;
var blackRGB;
var whiteRGB;

window.onresize = function (ev) {
	if (editor != undefined) {
		editor.view.resize();
		AreaPlugin.zoomAt(editor);
	}
}

async function startFlowProgramming() {
	// blackChannel = new cv.Mat(videoPlayer.videoHeight(), videoPlayer.videoWidth(), cv.CV_8UC1);
	// whiteChannel = new cv.Mat(videoPlayer.videoHeight(), videoPlayer.videoWidth(), cv.CV_8UC1);
	blackChannel = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC1);
	whiteChannel = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC1);
	whiteChannel.setTo([255, 255, 255, 255]);
	// blackRGB = new cv.Mat(videoPlayer.videoHeight(), videoPlayer.videoWidth(), cv.CV_8UC3);
	// whiteRGB = new cv.Mat(videoPlayer.videoHeight(), videoPlayer.videoWidth(), cv.CV_8UC3);
	blackRGB = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC3);
	whiteRGB = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC3);
	add(whiteRGB.data,255)
	whiteChannel.setTo([255, 255, 255, 255]);
	container = document.querySelector('#rete');
	// components = [new NumComponent(), new AddComponent()];
	components = {
		"Start_Node": new Start_Component(),
		"End_Node": new End_Component(),
		"Grayscale_Node": new Grayscale_Component(),
		"Split_Node": new Split_Component(),
		"Merge_Node": new Merge_Component(),
		"RGB2HSV_Node": new RGB2HSV_Component(),
		"HSV2RGB_Node": new HSV2RGB_Component(),
		"Channel255_Node": new Channel255_Component(),
		"Canny_Node": new Canny_Component(),
		"Convolution_Node": new Convolution_Component()
	};

	editor = new Rete.NodeEditor('demo@0.1.0', container);
	editor.use(ConnectionPlugin.default);
	editor.use(VueRenderPlugin.default);
	editor.use(ContextMenuPlugin.default);
	editor.use(AreaPlugin);
	editor.use(CommentPlugin.default);
	editor.use(HistoryPlugin.default);
	editor.use(ConnectionMasteryPlugin.default);
	editor.use(AutoArrangePlugin.default, { margin: {x: 100, y: 100 }, depth: 0 });

	engine = new Rete.Engine('demo@0.1.0');

	for (let key in components) {
		editor.register(components[key]);
		engine.register(components[key]);
	}

	// let startNode = await components["Start_Node"].createNode();
	// let endNode = await components["End_Node"].createNode();
	// let grayNode = await components["Grayscale_Node"].createNode();
	// let convolutionNode = await components["Convolution_Node"].createNode();
	// let mergeNode = await components["Merge_Node"].createNode();

	// n1.position = [80, 200];
	// n2.position = [80, 400];
	// add.position = [500, 240];
	// startNode.position = [0, 0];
	// grayNode.position = [250, 0];
	// convolutionNode.position = [500, 0];
	// mergeNode.position = [750, 0];
	// endNode.position = [1000, 0];


	// editor.addNode(n1);
	// editor.addNode(n2);
	// editor.addNode(add);
	// editor.addNode(startNode);
	// editor.addNode(grayNode);
	// editor.addNode(convolutionNode);
	// editor.addNode(mergeNode);
	// editor.addNode(endNode);

	// editor.connect(n1.outputs.get('num'), add.inputs.get('num'));
	// editor.connect(n2.outputs.get('num'), add.inputs.get('num2'));
	// editor.connect(startNode.outputs.get("RGB"), grayNode.inputs.get("RGB"));
	// editor.connect(grayNode.outputs.get("gray"), convolutionNode.inputs.get("channel-input"));
	// editor.connect(convolutionNode.outputs.get("convolved"), mergeNode.inputs.get("red"));
	// editor.connect(convolutionNode.outputs.get("convolved"), mergeNode.inputs.get("green"));
	// editor.connect(convolutionNode.outputs.get("convolved"), mergeNode.inputs.get("blue"));
	// editor.connect(mergeNode.outputs.get("RGB"), endNode.inputs.get("RGB"));

	await editor.fromJSON(editor_presets["basic"]);


	editor.on('process nodecreated noderemoved connectioncreated connectionremoved', async () => {
		// console.log('process');
		await engine.abort();
		await engine.process(editor.toJSON());
	});

	editor.view.resize();
	AreaPlugin.zoomAt(editor);
	editor.trigger('process');

	// setTimeout(toTest, 2000);
	// toTest();
}


//---------------------------------------------------------------------------------------------------------
function getSameIdIndx(node) {
	for (let i = 0; i < editor.nodes.length; i++) {
		if (node.id == editor.nodes[i].id) {
			return i;
		}
	}
}
function updateName(node, inputs, key) {
	let indx = getSameIdIndx(node);
	if (inputs[key].length != 0) {
		// input is connected
		if (inputs[key][0].newName != undefined) {
			// newName is specified

			if (editor.nodes[indx].inputs.get(key).name != inputs[key][0].newName) {
				// name should be changed
				editor.nodes[indx].inputs.get(key).name = inputs[key][0].newName;
				editor.nodes[indx].update();
			}
			else {
				// it already has correct name
			}
		}
		else {
			// newName isn't specified
		}
	}
	else {
		// input isn't connected
		if (editor.nodes[indx].inputs.get(key).name != key) {
			// name should be changed back to original
			editor.nodes[indx].inputs.get(key).name = key;
			editor.nodes[indx].update();
		}
		else {
			// it already has correct name
		}
	}
}
function getChannel(image, channel_indx) {
	// no given attribute is changed !!!
	let tempMatVector = new cv.MatVector();
	cv.split(image, tempMatVector);
	channel = tempMatVector.get(channel_indx);
	tempMatVector.delete();
	return channel;
}
function analyse_frame(destinationToDraw, type, src, accumulate, channels, histSize, ranges, hist, mask, color, scale) {
	// no given attribute is deleted !!!
	// src is persumed to be RGB
	// src is overriden !!!
	switch (type) {
		case "gray": cv.cvtColor(src, src, cv.COLOR_RGB2GRAY); break;
		case "red":
			let red = getChannel(src, 0);
			red.copyTo(src);
			red.delete();
			break;
		case "green":
			let green = getChannel(src, 1);
			green.copyTo(src);
			green.delete();
			break;
		case "blue":
			let blue = getChannel(src, 2);
			blue.copyTo(src);
			blue.delete();
			break;
		case "hue":
			cv.cvtColor(src, src, cv.COLOR_RGB2HSV_FULL);
			let hue = getChannel(src, 0);
			hue.copyTo(src);
			hue.delete();
			break;
		case "saturation":
			cv.cvtColor(src, src, cv.COLOR_RGB2HSV_FULL);
			let saturation = getChannel(src, 1);
			saturation.copyTo(src);
			saturation.delete();
			break;
		case "value":
			cv.cvtColor(src, src, cv.COLOR_RGB2HSV_FULL);
			let value = getChannel(src, 2);
			value.copyTo(src);
			value.delete();
			break;
		default: print("Given analysis type is not implemented!"); return;
	}
	let srcVec = new cv.MatVector();
	srcVec.push_back(src);

	cv.calcHist(srcVec, channels, mask, hist, histSize, ranges, accumulate);
	let result = cv.minMaxLoc(hist, mask);
	let max = result.maxVal;
	// let max = src.rows*src.cols/4;
	// console.log(max)
	let dst = new cv.Mat.zeros(src.rows, histSize[0] * scale, cv.CV_8UC3);
	add(dst.data,73)

	for (let i = 0; i < histSize[0]; i++) {
		let binVal = hist.data32F[i] * src.rows / max;
		let point1 = new cv.Point(i * scale, src.rows - 1);
		let point2 = new cv.Point((i + 1) * scale - 1, src.rows - binVal);
		cv.rectangle(dst, point1, point2, color, cv.FILLED);
	}
	cv.imshow(destinationToDraw, dst);
	dst.delete(); srcVec.delete();
}
function scale(arr, scalar) {
	let len = arr.length;
	for (let i = 0; i < len; i++) {
		arr[i] *= scalar;
	}
}
function add(arr, scalar) {
	let len = arr.length;
	for (let i = 0; i < len; i++) {
		arr[i] += scalar;
	}
}
// var numSocket = new Rete.Socket('Number value');
var Mat1_socket = new Rete.Socket('Mat1');
var Mat3_socket = new Rete.Socket('Mat3');

//---------------------------------------------------------------------------------------------------------
class Start_Component extends Rete.Component {
	constructor() {
		super("Video");
		this.data.render = 'vue';
	}
	builder(node) {
		let output_1 = new Rete.Output("RGB", "RGB", Mat3_socket);
		node.data.rgb_frame_holder = new cv.Mat();

		node.data.analysis_src = new cv.Mat();
		node.data.analysis_accumulate = false;
		node.data.analysis_channels = [0];
		node.data.analysis_histSize = [256];
		node.data.analysis_ranges = [0, 255];
		node.data.analysis_hist = new cv.Mat();
		node.data.analysis_mask = new cv.Mat();
		node.data.analysis_color = new cv.Scalar(103, 112, 219);
		// #677bdb
		node.data.analysis_scale = 2;

		return node.addOutput(output_1);
	}
	worker(node, inputs, outputs) {
		// print("----")
		// print(videoElement.width)
		// print(frameOriginal.size().height)
		// print("----")
		// print(videoElement.height)
		// print(frameOriginal.size().width)
		if(videoElement.height==0 || videoElement.width==0){
			outputs["RGB"] = node.data.rgb_frame_holder;
			return
		}
		// print(frameOriginal.size())
		// print(videoElement.height)
		// print(videoElement.width)
		// print(videoElement.videoHeight)
		// print(videoElement.videoWidth)
		cap.read(frameOriginal);
		cv.cvtColor(frameOriginal, node.data.rgb_frame_holder, cv.COLOR_RGBA2RGB);
		outputs["RGB"] = node.data.rgb_frame_holder;

		if (document.querySelector("#analysis-channel").value != "stop") {
			node.data.rgb_frame_holder.copyTo(node.data.analysis_src);
			analyse_frame(
				"original-analysis-canvas",
				document.querySelector("#analysis-channel").value,
				node.data.analysis_src,
				node.data.analysis_accumulate,
				node.data.analysis_channels,
				node.data.analysis_histSize,
				node.data.analysis_ranges,
				node.data.analysis_hist,
				node.data.analysis_mask,
				node.data.analysis_color,
				node.data.analysis_scale
			);
		}
	}
}
//---------------------------------------------------------------------------------------------------------
class End_Component extends Rete.Component {
	constructor() {
		super("Canvas");
		this.data.render = 'vue';
	}
	builder(node) {
		let input_1 = new Rete.Input("RGB", "RGB", Mat3_socket);

		node.data.analysis_src = new cv.Mat();
		node.data.analysis_accumulate = false;
		node.data.analysis_channels = [0];
		node.data.analysis_histSize = [256];
		node.data.analysis_ranges = [0, 255];
		node.data.analysis_hist = new cv.Mat();
		node.data.analysis_mask = new cv.Mat();
		node.data.analysis_color = new cv.Scalar(103, 112, 219);
		node.data.analysis_scale = 2;

		return node.addInput(input_1);
	}
	worker(node, inputs, outputs) {
		if (inputs["RGB"].length != 0) {
			cv.imshow("canvasOutput", inputs["RGB"][0]);
			if (document.querySelector("#analysis-channel").value != "stop") {
				inputs["RGB"][0].copyTo(node.data.analysis_src);
			}
			else {
				return;
			}
		}
		else {
			cv.imshow("canvasOutput", blackRGB);
			if (document.querySelector("#analysis-channel").value != "stop") {
				blackRGB.copyTo(node.data.analysis_src);
			}
			else {
				return;
			}
		}
		analyse_frame(
			"final-analysis-canvas",
			document.querySelector("#analysis-channel").value,
			node.data.analysis_src,
			node.data.analysis_accumulate,
			node.data.analysis_channels,
			node.data.analysis_histSize,
			node.data.analysis_ranges,
			node.data.analysis_hist,
			node.data.analysis_mask,
			node.data.analysis_color,
			node.data.analysis_scale
		);
	}
}
//---------------------------------------------------------------------------------------------------------
class Grayscale_Component extends Rete.Component {
	constructor() {
		super("Grayscale");
		this.data.render = 'vue';
	}

	builder(node) {
		let input_1 = new Rete.Input("RGB", "RGB", Mat3_socket);
		let output_1 = new Rete.Output("gray", "gray", Mat1_socket);
		node.data.gray_frame_holder = new cv.Mat();

		return node.addInput(input_1).addOutput(output_1);
	}

	worker(node, inputs, outputs) {
		if (inputs["RGB"].length == 0) {
			outputs["gray"] = blackChannel;
		}
		else {
			cv.cvtColor(inputs["RGB"][0], node.data.gray_frame_holder, cv.COLOR_RGB2GRAY);
			outputs["gray"] = node.data.gray_frame_holder;
		}
	}
}
//---------------------------------------------------------------------------------------------------------
class Split_Component extends Rete.Component {
	constructor() {
		super("Split");
		this.data.render = 'vue';
	}
	build(node) {

		let input_RGB = new Rete.Input("RGB", "RGB", Mat3_socket);

		node.data.R = new cv.Mat();
		node.data.G = new cv.Mat();
		node.data.B = new cv.Mat();
		let output_R = new Rete.Output("red", "red", Mat1_socket);
		let output_G = new Rete.Output("green", "green", Mat1_socket);
		let output_B = new Rete.Output("blue", "blue", Mat1_socket);

		return node.addInput(input_RGB).addOutput(output_R).addOutput(output_G).addOutput(output_B);
	}

	worker(node, inputs, outputs) {
		if (inputs["RGB"].length == 0) {
			outputs["red"] = blackChannel;
			outputs["green"] = blackChannel;
			outputs["blue"] = blackChannel;
		}
		else {
			let tempMatVector = new cv.MatVector();
			cv.split(inputs["RGB"][0], tempMatVector);


			node.data.R.delete();
			node.data.G.delete();
			node.data.B.delete();
			node.data.R = tempMatVector.get(0);
			node.data.G = tempMatVector.get(1);
			node.data.B = tempMatVector.get(2);

			tempMatVector.delete();

			outputs["red"] = node.data.R;
			outputs["green"] = node.data.G;
			outputs["blue"] = node.data.B;

		}
		// print(this.R)

	}
}
//---------------------------------------------------------------------------------------------------------
class Merge_Component extends Rete.Component {
	constructor() {
		super("Merge");
		this.data.render = 'vue';
	}
	build(node) {
		node.data.RGB = new cv.Mat();

		let input_R = new Rete.Input("red", "red", Mat1_socket);
		let input_G = new Rete.Input("green", "green", Mat1_socket);
		let input_B = new Rete.Input("blue", "blue", Mat1_socket);

		let output_RGB = new Rete.Output("RGB", "RGB", Mat3_socket);

		return node.addInput(input_R).addInput(input_G).addInput(input_B).addOutput(output_RGB);
	}

	worker(node, inputs, outputs) {
		let R;
		let G;
		let B;
		// frame for red spectrum
		if (inputs["red"].length == 0) {
			R = blackChannel;
		}
		else {
			R = inputs["red"][0];
			node.data.r = true;
		}

		// frame for green spectrum
		if (inputs["green"].length == 0) {
			G = blackChannel;
		}
		else {
			G = inputs["green"][0];
		}

		// frame for blue spectrum
		if (inputs["blue"].length == 0) {
			B = blackChannel;
		}
		else {
			B = inputs["blue"][0];
		}

		let tempMatVector = new cv.MatVector();
		tempMatVector.push_back(R);
		tempMatVector.push_back(G);
		tempMatVector.push_back(B);
		cv.merge(tempMatVector, node.data.RGB);
		outputs["RGB"] = node.data.RGB;

		tempMatVector.delete();
	}
}
//---------------------------------------------------------------------------------------------------------
class RGB2HSV_Component extends Rete.Component {
	constructor() {
		super("RGB → HSV");
		this.data.render = 'vue';
	}
	build(node) {
		node.data.hsv_frame_holder = new cv.Mat();

		let input_1 = new Rete.Input("RGB", "RGB", Mat3_socket);
		let output_1 = new Rete.Output("HSV", "HSV", Mat3_socket);

		return node.addInput(input_1).addOutput(output_1);
	}
	worker(node, inputs, outputs) {
		if (inputs["RGB"].length == 0) {
			outputs["HSV"] = blackRGB;
		}
		else {
			cv.cvtColor(inputs["RGB"][0], node.data.hsv_frame_holder, cv.COLOR_RGB2HSV_FULL);
			outputs["HSV"] = node.data.hsv_frame_holder;
		}
	}
}
//---------------------------------------------------------------------------------------------------------
class HSV2RGB_Component extends Rete.Component {
	constructor() {
		super("HSV → RGB");
		this.data.render = 'vue';
	}
	build(node) {
		node.data.rgb_frame_holder = new cv.Mat();

		let input_1 = new Rete.Input("HSV", "HSV", Mat3_socket);
		let output_1 = new Rete.Output("RGB", "RGB", Mat3_socket);

		return node.addInput(input_1).addOutput(output_1);
	}
	worker(node, inputs, outputs) {
		if (inputs["HSV"].length == 0) {
			outputs["RGB"] = blackRGB;
		}
		else {
			cv.cvtColor(inputs["HSV"][0], node.data.rgb_frame_holder, cv.COLOR_HSV2RGB_FULL);
			outputs["RGB"] = node.data.rgb_frame_holder;
		}
	}
}
//---------------------------------------------------------------------------------------------------------
class Channel255_Component extends Rete.Component {
	constructor() {
		super("255 Channel");
		this.data.render = 'vue';
	}
	build(node) {
		let output_1 = new Rete.Output("255", "255", Mat1_socket);
		return node.addOutput(output_1);
	}
	worker(node, inputs, outputs) {
		outputs["255"] = whiteChannel;
	}

}
//---------------------------------------------------------------------------------------------------------
class Canny_Component extends Rete.Component {
	constructor() {
		super("Canny edge");
		this.data.render = 'vue';
	}
	build(node) {
		let input_1 = new Rete.Input("RGB", "RGB", Mat3_socket);
		let output_1 = new Rete.Output("edge", "edge", Mat1_socket);

		node.data.canny_frame_holder = new cv.Mat();
		return node.addInput(input_1).addOutput(output_1);
	}
	worker(node, inputs, outputs) {
		if (inputs["RGB"].length == 0) {
			outputs["edge"] = blackChannel;
		}
		else {
			cv.Canny(inputs["RGB"][0], node.data.canny_frame_holder, 50, 100, 3, false);
			outputs["edge"] = node.data.canny_frame_holder;
		}
	}
}
//---------------------------------------------------------------------------------------------------------
class Convolution_Component extends Rete.Component {
	constructor() {
		super("9x9 average convolution");
		this.data.render = 'vue';
	}
	build(node) {
		let input_1 = new Rete.Input("channel-input", "channel-input", Mat1_socket);
		let output_1 = new Rete.Output("convolved", "convolved", Mat1_socket);

		node.data.convolved_frame_holder = new cv.Mat();
		let size = 9;
		node.data.kernel = cv.Mat.ones(size, size, cv.CV_32FC1);
		scale(node.data.kernel.data32F,1/(size*size)); // avereging kernel
		node.data.anchor = new cv.Point(-1,-1); // center

		return node.addInput(input_1).addOutput(output_1);
	}
	worker(node, inputs, outputs) {
		if (inputs["channel-input"].length == 0) {
			outputs["convolved"] = blackChannel;
		}
		else {
			// You can try more different parameters
			cv.filter2D(inputs["channel-input"][0], node.data.convolved_frame_holder, cv.CV_8U, node.data.kernel, node.data.anchor, 0, cv.BORDER_DEFAULT );
			
			outputs["convolved"] = node.data.convolved_frame_holder;
		}
	}
}
//---------------------------------------------------------------------------------------------------------

// function downloadObjectAsJson(exportObj, exportName){
//     var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
//     var downloadAnchorNode = document.createElement('a');
//     downloadAnchorNode.setAttribute("href",     dataStr);
//     downloadAnchorNode.setAttribute("download", exportName + ".json");
//     // document.body.appendChild(downloadAnchorNode); // required for firefox
//     downloadAnchorNode.click();
//     downloadAnchorNode.remove();
//   }
