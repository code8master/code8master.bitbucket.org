var videoElement;
// var videoPlayer;
var videoToCapture;
var cap;

var frameOriginal;


var counter = 0;

opencvLoadedFlag = false;

//------------------------------------------------------------------------------------
function opencvLoaded() {
	// document.querySelector("#status").innerHTML = "OpenCV.js is ready.";
	opencvLoadedFlag = true;
}
//------------------------------------------------------------------------------------
function changeEditor(preset_name){
	if (preset_name in editor_presets){
		editor.fromJSON(editor_presets[preset_name]);
	}
	else {
		editor.fromJSON(editor_presets["basic"]);
	}
	setTimeout(focusEditor, 50);
}
function focusEditor(){
	editor.view.resize();
	AreaPlugin.zoomAt(editor);
}
//------------------------------------------------------------------------------------
function startAnalysis(){
	if (document.querySelector("#analysis-channel").value == "stop"){
		document.querySelector("#original-video-row").setAttribute("class","row height-16-16");
		document.querySelector("#final-canvas-row").setAttribute("class","row height-16-16");
		document.querySelector("#original-analysis-row").setAttribute("class","row height-0-16");
		document.querySelector("#final-analysis-row").setAttribute("class","row height-0-16");
	}
	else {
		document.querySelector("#original-video-row").setAttribute("class","row height-12-16");
		document.querySelector("#final-canvas-row").setAttribute("class","row height-12-16");
		document.querySelector("#original-analysis-row").setAttribute("class","row height-4-16");
		document.querySelector("#final-analysis-row").setAttribute("class","row height-4-16");
	}
	editor.trigger('process');
}
//------------------------------------------------------------------------------------
inputBlob = "";
function inputChanged(event) {
	let files = document.querySelector("#videoInput").files;
	if (inputBlob.startsWith("blob")) {
		URL.revokeObjectURL(inputBlob);
	}
	videoElement.cancelVideoFrameCallback(frameCallbackToCancel);
	document.querySelector("#videoSrc").remove();
	if (files.length != 0) {
		inputBlob = URL.createObjectURL(files[0]);

		document.querySelector("#insertVideo").innerHTML = `
		<video id="videoSrc" class="position-absolute top-50 start-50 translate-middle" controls muted loop>
			<source src="`+ inputBlob + `">
			Your browser does not support this type of video.
		</video>`;
	}
	else {
		document.querySelector("#insertVideo").innerHTML = `
		<video id="videoSrc" class="position-absolute top-50 start-50 translate-middle" controls muted loop>
			<source src="./startVideo.mp4" id="video_here">
			Your browser does not support this type of video.
		</video>`;
	}
	videoElement = document.getElementById("videoSrc");
	lastFrameCount = -1;
	videoElement.load();
	videoElement.play();
	videoElement.requestVideoFrameCallback(processVideo);
}
//------------------------------------------------------------------------------------
function print(el) {
	console.log(el);
}
//------------------------------------------------------------------------------------
function error(el) {
	console.error(el);
}
//------------------------------------------------------------------------------------

window.onload = function () {

	document.querySelector("#videoInput").addEventListener("change", inputChanged);
	if (opencvLoadedFlag == false) {
		console.error("OpenCV hasn't been loadaed yet !!!");
	}
	console.log("Window Loaded");
	// console.log("dobljeno: "+cv.getBuildInformation());
	cv['onRuntimeInitialized'] = () => {
		// do all your work here

		videoElement = document.querySelector("#videoSrc");

		//! videoPlayer = videojs("videoSrc");

		//! videoElement = videoPlayer.children_[0];

		// videoToCapture = document.createElement("video");
		// videoToCapture.setAttribute("src",videoPlayer.currentSource().src);

		// videoPlayer.on("loadedmetadata",mainF);
		setTimeout(mainF, 100);
	};
}

//------------------------------------------------------------------------------------

function mainF() {
	// print("checkPoint1");
	// videoToCapture.setAttribute("height",videoPlayer.videoHeight());
	// videoToCapture.setAttribute("width",videoPlayer.videoWidth());
	// videoToCapture.setAttribute("controls",true);
	// videoToCapture.setAttribute("muted",true);
	// document.querySelector(".inputoutput").appendChild(videoToCapture);
	videoElement.height = videoElement.videoHeight
	videoElement.width = videoElement.videoWidth
	cap = new cv.VideoCapture(videoElement);

	// print("checkPoint2");

	//! frameOriginal = new cv.Mat(videoPlayer.videoHeight(), videoPlayer.videoWidth(), cv.CV_8UC4);
	frameOriginal = new cv.Mat(videoElement.height, videoElement.width, cv.CV_8UC4);

	startFlowProgramming();

	// print("checkPoint3");
	// window.requestAnimationFrame(processVideo);
	// setTimeout(processVideo, 0);
	// videoElement.requestVideoFrameCallback(processVideo);
	// videoPlayer.currentTime(0);
	// videoPlayer.playbackRate(0.1);
	// videoPlayer.on("seeked",()=>{counter=0;});
	processVideo()
}

//------------------------------------------------------------------------------------
lastFrameCount = -1;
updatePerNewMonitorFrame = false;
frameCallbackToCancel = 1;

function processVideo(atr1, atr2) {
	// print("-------")
	// print(lastFrameCount);
	// print(videoElement.width)
	// print(videoElement.videoWidth)
	// print(videoElement.height)
	// print(videoElement.videoHeight)
	if (videoElement.width != videoElement.videoWidth || videoElement.height != videoElement.videoHeight) {
		videoElement.width = videoElement.videoWidth;
		videoElement.height = videoElement.videoHeight;
		frameOriginal.delete();
		frameOriginal = new cv.Mat(videoElement.height, videoElement.width, cv.CV_8UC4);
		cap = new cv.VideoCapture(videoElement);
		
		blackChannel.delete();
		blackChannel = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC1);
		scale(blackChannel.data,0);
		whiteChannel.delete();
		whiteChannel = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC1);
		whiteChannel.setTo([255, 255, 255, 255]);
		blackRGB.delete();
		blackRGB = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC3);
		scale(blackRGB.data,0);
		whiteRGB.delete();
		whiteRGB = new cv.Mat(videoElement.videoHeight, videoElement.videoWidth, cv.CV_8UC3);
		add(whiteRGB.data,255)
	}
	// print("atr1")
	// print(atr1)
	// print("atr2")
	// print(atr2)
	counter++;
	// print(counter);
	// print(lastFrameCount)
	// print("---")
	//! frameCountCandidate = videoPlayer.getVideoPlaybackQuality().totalVideoFrames;

	if (atr2 == undefined) {
		frameCountCandidate = videoElement.getVideoPlaybackQuality().totalVideoFrames;
	}
	else {
		frameCountCandidate = atr2.presentedFrames;
	}

	if (lastFrameCount == frameCountCandidate && updatePerNewMonitorFrame) {
		// print("---> same");
	}
	else {
		// print("new");
		lastFrameCount = frameCountCandidate;

		// cap.read(frameOriginal);

		// let operations = ["toGrayScale"];
		// let frame = manipulateFrame(frameOriginal.clone(), operations);
		// cv.imshow("canvasOutput", frame);
		// frame.delete();

		editor.trigger('process');
	}

	// window.requestAnimationFrame(processVideo);
	frameCallbackToCancel = videoElement.requestVideoFrameCallback(processVideo);
}
//------------------------------------------------------------------------------------
function openNav() {
	document.getElementById("myNav").style.display = "block";
}
//------------------------------------------------------------------------------------
function closeNav() {
	document.getElementById("myNav").style.display = "none";
}
//------------------------------------------------------------------------------------






/*

!				ANALYSIS of OpenCV.js documentation

!https://docs.opencv.org/master/df/df7/tutorial_js_table_of_contents_setup.html
- Introduction to OpenCV.js and Tutorials
- Using OpenCV.js
- Build OpenCV.js
- Using OpenCV.js In Node.js
	- Minimal example
	- Working with images

!https://docs.opencv.org/master/df/d04/tutorial_js_table_of_contents_gui.html
- Getting Started with Images
	- Read an image
	- Display an image
	- In OpenCV.js
- Getting Started with Videos
	- Capture video from camera
	- Playing video
- Add a Trackbar to Your Application
	- Code Demo

!https://docs.opencv.org/master/de/d06/tutorial_js_basic_ops.html
- Accessing Image Properties
- How to construct Mat
- How to copy Mat
- How to convert the type of Mat
- How use MatVector
- Accessing and Modifying pixel values
- Image ROI
- Splitting and Merging Image Channels
- Making Borders for Images (Padding)

!https://docs.opencv.org/master/dd/d4d/tutorial_js_image_arithmetics.html
- Image Addition
- Image Subtraction
- Bitwise Operations

!https://docs.opencv.org/master/d5/df1/tutorial_js_some_data_structures.html
- Point
- Scalar
- Size
- Circle
- Rect
- RotatedRect

!https://docs.opencv.org/master/db/d64/tutorial_js_colorspaces.html
- cvtColor
- inRange

!https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html#ga4e0972be5de079fed4e3a10e24ef5ef0
- Color Conversion Codes

!https://docs.opencv.org/master/dd/d52/tutorial_js_geometric_transformations.html
- Transformations
	- Scaling
	- Translation
	- Rotation
	- Affine Transformation
	- Perspective Transformation

!https://docs.opencv.org/master/d7/dd0/tutorial_js_thresholding.html
- Simple Thresholding
- Adaptive Thresholding

!https://docs.opencv.org/master/dd/d6a/tutorial_js_filtering.html
- 2D Convolution ( Image Filtering )
- Image Blurring (Image Smoothing)
	- 1. Averaging
	- 2. Gaussian Blurring
	- 3. Median Blurring
	- 4. Bilateral Filtering

!https://docs.opencv.org/master/d4/d76/tutorial_js_morphological_ops.html
	- 1. Erosion
	- 2. Dilation
	- 3. Opening
	- 4. Closing
	- 5. Morphological Gradient
	- 6. Top Hat
	- 7. Black Hat
- Structuring Element

!https://docs.opencv.org/master/da/d85/tutorial_js_gradients.html
- 1. Sobel and Scharr Derivatives
- 2. Laplacian Derivatives
	- CV_8U will clip negative values and values above 255 !!!

!https://docs.opencv.org/master/d7/de1/tutorial_js_canny.html
- Canny Edge Detection in OpenCV

!https://docs.opencv.org/master/d5/d0f/tutorial_js_pyramids.html
- Downsample
- Upsample

!https://docs.opencv.org/master/d0/d43/tutorial_js_table_of_contents_contours.html
- Contours
- Contour Approximation Method
- Features
	- 1. Moments
	- 2. Contour Area
	- 3. Contour Perimeter
	- 4. Contour Approximation
	- 5. Convex Hull
	- 6. Checking Convexity
	- 7. Bounding Rectangle
	- 8. Minimum Enclosing Circle
	- 9. Fitting an Ellipse
	- 10. Fitting a Line
- Properties
	- 1. Aspect Ratio
	- 2. Extent
	- 3. Solidity
	- 4. Equivalent Diameter
	- 5. Orientation
	- 6. Mask and Pixel Points
	- 7. Maximum Value, Minimum Value and their locations
	- 8. Mean Color or Mean Intensity
- More Functions
	- 1. Convexity Defects
	- 2. Point Polygon Test
	- 3. Match Shapes
- Hierarchy
	- Contour Retrieval Mode

!https://docs.opencv.org/master/d5/dc0/tutorial_js_table_of_contents_histograms.html
- Histograms
- Find, Plot, Analyze !!!
- Find Histogram
- Histogram Equalization
- CLAHE (Contrast Limited Adaptive Histogram Equalization)
- Histogram Backprojection

!https://docs.opencv.org/master/dd/d02/tutorial_js_fourier_transform.html
- Fourier Transform in OpenCV

!https://docs.opencv.org/master/d8/dd1/tutorial_js_template_matching.html
- Template Matching

!https://docs.opencv.org/master/d3/de6/tutorial_js_houghlines.html
- Hough Transform in OpenCV
- Hough Line Transform
- Probabilistic Hough Transform

!https://docs.opencv.org/master/d3/de5/tutorial_js_houghcircles.html
- Hough Circle Transform

!https://docs.opencv.org/master/d7/d1c/tutorial_js_watershed.html
- Image Segmentation with Watershed Algorithm

!https://docs.opencv.org/master/dd/dfc/tutorial_js_grabcut.html
- Foreground Extraction using GrabCut Algorithm

!https://docs.opencv.org/master/d8/d54/tutorial_js_imgproc_camera.html
- Image Processing for Video Capture

!https://docs.opencv.org/master/d9/df5/tutorial_js_intelligent_scissors.html
- Intelligent Scissors Demo

!https://docs.opencv.org/master/df/def/tutorial_js_meanshift.html
- Meanshift
- Camshift

!https://docs.opencv.org/master/db/d7f/tutorial_js_lucas_kanade.html
- Optical Flow
- Lucas-Kanade Optical Flow in OpenCV.js
- Dense Optical Flow in OpenCV.js

!https://docs.opencv.org/master/de/df4/tutorial_js_bg_subtraction.html
- BackgroundSubtractorMOG2

!https://docs.opencv.org/master/d2/d99/tutorial_js_face_detection.html
- Face Detection using Haar Cascades

!https://docs.opencv.org/master/df/d6c/tutorial_js_face_detection_camera.html
- Face Detection in Video Capture

!https://docs.opencv.org/master/d0/db7/tutorial_js_table_of_contents_dnn.html
- Image Classification Example
	- DNN module
	- caffe models
	- tensorflow model
- Image Classification Example with Camera
- Object Detection Example
- Object Detection Example with Camera
- Semantic Segmentation Example
- Style Transfer Example
- Pose Estimation Example

*/